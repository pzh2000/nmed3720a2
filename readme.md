# Knife
This is a microsite created by Michael, Alex and Colin for the Blackfoot Digital Library. 
We used A-Frame (https://aframe.io/) and A-Frame orbit controls (https://unpkg.com/) to display a GLTF model (created using photogrammetry at the University of Lethbridge).


The site aligns with the Blackfoot Digital Library's mission to honour Akaitapii.

The object portrayed is a knife made by the Blackfoot.

#Copyright
Object portrayed is property of the Blackfoot People, served exclusively by the Blackfoot Digital Library in Lethbridge, AB.

Background photo captured by Ruine Fragenstein on Flickr (https://www.flickr.com/photos/photo-pie/).

Copyright is held under the Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0) License.